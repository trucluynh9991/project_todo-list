import React, {useState, useEffect} from 'react'
import { useDispatch, useSelector } from "react-redux";
import { Router, Switch, Route, Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import Login from "./commans/members/login/Login";
import Register from "./commans/members/registers/Register";

import Profile from "./commans/members/account/Account";

import { logout } from "./redux/actions/auth";
import { clearMessage } from "./redux/actions/message";

import { history } from "./history";

const App = () => {
  // const { user: currentUser } = useSelector((state) => state.auth);
  // const dispatch = useDispatch();

  // useEffect(() => {
  //   const id = 1
   
  //   history.listen((location) => {
  //     dispatch(clearMessage()); // clear message when changing location
  //   });
  // }, [dispatch]);


  // const logOut = () => {
  //   dispatch(logout());
  // };

  return (
    <Router history={history}>
      <div>
    
        <div className="container mt-3">
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
          </Switch>
        </div>
      </div>
    </Router>
  );
};

export default App;