import axios from 'axios';
export const API_URL = "https://api-nodejs-todolist.herokuapp.com/user/";

const register = (name, email, password) => {
    return axios.post(API_URL + "register", {
      name,
      email,
      password,
    }) 
  };
  
  const login = (email, password) => {
    return axios
      .post(API_URL + "login", {
        email,
        password,
      })
      .then((response) => {
        if (response.data.token) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }
  
        return response.data;
      });
  };
  
  const logout = () => {
    localStorage.removeItem("user");
  };
  
  export default {
    register,
    login,
    logout,
  };