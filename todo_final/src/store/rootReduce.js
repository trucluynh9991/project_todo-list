import { combineReducers } from "redux";
import auth from "../redux/reduces/auth";
import message from "../redux/reduces/message";

const rootReducer =  combineReducers({
  auth,
  message,
});
export default rootReducer;