import axios from 'axios'

export const API_URL = "https://api-nodejs-todolist.herokuapp.com/user/";

export default function callApi(endpoint, method = 'GET', body){
    
    return axios({ 
        method: method,
        url: `${API_URL}/${endpoint}?_embed=todos`,
        data: body
        
    }).catch(err =>{
        console.log(err)
    })
}